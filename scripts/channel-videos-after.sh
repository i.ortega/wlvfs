#!/usr/bin/env sh

channel="$1"
last_watched_video="$2"
scripts_folder=$(dirname "$(readlink -f "$0")")

[ -n "$3" ] && dateafter="--dateafter $3"


fifo="/tmp/channel-videos-after-fifo$(date +%s%N)"
mkfifo "$fifo"

youtube-dl --get-id "$channel" $dateafter 2>/dev/null > "$fifo" &
ytdlp="$!"
while read -r line && [ "$line" != "$last_watched_video" ]; do
    echo "$line"
done < "$fifo"

printf "\n"
kill "$!"

# youtube-dl --get-id "$channel" | grep -B 4 "$last_watched_video" | tee /home/initega/test/test -a &

# child_pid=$!

# echo test/test | entr -p kill $child_pid
